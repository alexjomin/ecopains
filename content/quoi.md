---
title: "C’est quoi les écopains ?"
date: 2020-09-06T08:05:07+02:00
draft: false
---

Les écopains c’est un regroupement d’enfants de 8 à 12 ans qui aiment la nature
et l’écologie. 

Les écopains propose des ateliers, des activités, des sorties et
même un week end à la fin de l’année! Nous créons des produits écolos, nous
utilisons du matériel de récup et nous faisons des jeux dans et avec la nature.

Bien sûr nous ne sommes pas seuls, nous sommes aidés par 2 mamans qui nous
donnent des coup de mains et des idées.

Nous partageons aussi nos découvertes et nos pratiques écologiques avec nos
parents qui participeront un peu à chacun de nos ateliers.

Ce qui est important pour être un écopain: le respect des autres et de la
nature, la joie, le partage, l’amitié et l’envie d’être ensemble.
